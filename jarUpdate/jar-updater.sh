#!/usr/bin/env bash
######################################
##                  ##
## Jar file downloader/updater    ##
##                  ##
######################################
#constant vars/
export HOST_TO_PING="localhost"
export JAR_URL="http://existanze.com"
export DOCKER_CONTAINER_IP="localhost"

#set -x; #for debug
#test with localhost
wait_for_ping () {
  while true;
  do ping -c1 ${HOST_TO_PING} > /dev/null && break;
  done;
  echo "Conection found...";
}
#ssh host@ip #establish ssh connection with server or host to download file

#check and wait until file is downloaded// sleep 10 if internet connection i slow. 
download_file () {
  while true
  do
    STATUS=$(curl -s -o ~/Desktop/file.jar -w '%{http_code}' ${JAR_URL} )
    if [ $STATUS -eq 200 ]; then
      echo "Download complete"
      break
    else
      echo "Download pending..."
    fi
    sleep 10
  done ;
}

update_jar () {
  new_jarfile=$(sha1sum ~/Desktop/file.jar);
  old_jarfile=$(sha1sum ~/Desktop/old_file.jar);
}

docker_check () {
  #start docker elegxos and re-run script if nothing happens.
  if [ `systemctl is-active docker` = "active" ]; 
    then echo "Docker is active";
      else echo "Trying to restart docker..."; 
        systemctl stop docker; systemctl start docker; sleep 10;
       #check again to see if docker started
      if [ `systemctl is-active docker` = "active" ]; 
        then 
          echo "Docker is active";
        else
            echo "ERROR !!! Docker failed to start. Please contact the system administrator!"; exit 0 ;
      fi;
  fi;

}


docker_compose_check () {

  docker-compose start ; sleep 10;
  ((count = 10))
  while [[ $count -ne 0 ]] ; do
      ping -c 1 $DOCKER_CONTAINER_IP > /dev/null
      rc=$?
      if [[ $rc -eq 0 ]] ; then
          ((count = 1))
      fi
      ((count = count - 1))
  done

  if [[ $rc -eq 0 ]] ; then
      echo "Docker container is up and running!"
  else
      echo "Docker container failed to start or there is no connection!"; exit 0;
  fi
}

#exec wait_for_ping
export -f wait_for_ping
timeout 10s bash -c wait_for_ping
exit_status0=$?
if [[ $exit_status0 -eq 124 ]]; then
    echo "Connection timed out! Faild to ping host."; exit 0;
fi
#exec download_file
export -f download_file
timeout 10s bash -c download_file
exit_status1=$?
if [[ $exit_status1 -eq 124 ]]; then
    echo "File failed to download. Connection timed out."; exit 0;
fi

#timeout --foreground -k 10s wait_for_ping;
#timeout --foreground -k 10s download_file;

docker_check;
docker_compose_check;

echo "All good"
exit 0 
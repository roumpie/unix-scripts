#!/bin/bash
export DOCKER_CONTAINER_IP="localhost"

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

main () {
  echo "------------#---------------"
  
    #create backup dir if it doesn't exist
    mkdir -p /var/backups/postgres_db/ ;
    # Location to place backups.
    backup_dir="/var/backups/postgres_db/" ;
    #
    #this will delete all the backups that are 10 days or older.
    delete_after_days="+10";
    find $backup_dir -mtime $delete_after_days -type f -delete
    # read db_name ---> to get the name of the container running postgresql on docker
    # read db_user ---> to log in the db as specific user.
    db_name="container-name" ;
    db_user="username" ;
    #cd /var/backups/postgres_db/ ; go to backup folder
    cd $backup_dir
    echo "------------#---------------"
    echo "Press 1 to BACKUP database"
    echo "Press 2 to RESTORE a backup"
    echo "------------#---------------"
    echo -e "${RED}Press enything else to exit${NC}"
    read user_input ;
    
    while true
    do
      if [ "$user_input" = "1" ]
      then
        clear;
        # to commpres xz file and restore backup
        docker exec -u postgres $db_name pg_dumpall -c | xz > $db_name-backup-$(date -u +%Y-%m-%d_%H-%M).sql.xz 
        #docker exec -t $db_name pg_dumpall -c -U postgres > dump_`date +%Y-%m-%d_%H-%M`.sql.gz
        echo "Back up complete";
        return;
      elif [ "$user_input" = "2" ]
      then
        clear;
        echo "------------#---------------"
        echo "Available backups: "
        echo
        cd /var/backups/postgres_db/
        echo -e "${GREEN}"
        ls
        echo -e "${NC}"
        echo "------------#---------------"
        echo "Enter the back up file name you want to restore: "
        echo "------------#---------------"
        read backup_name;
        if [ -f "$backup_name" ]
        then

          #if xz zipping //
          xz -dc $backup_name | docker exec -i -u postgres $db_name psql 
          
          #cat $backup_name | docker exec -i $db_name psql -U postgres
          echo "--------------#------------------"
          echo "Restored succesfully"
          return;
        else
          clear;
          echo "------------#---------------"
          echo -e "${RED}File does not exist!${NC}"
          echo "----------------------#-----------------------"
          echo "Press 1 to retry:"
          echo "----------------------#-----------------------"
          read retry_input

          if [ "$retry_input" = "1" ]
          then
            continue;
          else
            echo "Bye Bye"
            return;
          fi;
        fi;
      else
        exit 1;
      fi;
    done;

}

docker_check () {
  #start docker elegxos and re-run script if nothing happens.
  if [ `systemctl is-active docker` = "active" ]; 
    then echo "Docker is active";
      else 
        echo -e "${RED}Docker not active!!!${NC}"
        echo
        echo "Trying to restart docker..."; 
        systemctl stop docker; systemctl start docker; sleep 5;
       #check again to see if docker started
      if [ `systemctl is-active docker` = "active" ]; 
        then 
          echo "Docker is active";
        else
            echo -e "${RED}ERROR !!! Docker failed to start. Please contact the system administrator!"${NC}; exit 0 ;
      fi;
  fi;

}

docker_compose_check () {

  docker-compose start ; sleep 5;
  ((count = 10))
  while [[ $count -ne 0 ]] ; do
      ping -c 1 $DOCKER_CONTAINER_IP > /dev/null
      rc=$?
      if [[ $rc -eq 0 ]] ; then
          ((count = 1))
      fi
      ((count = count - 1))
  done

  if [[ $rc -eq 0 ]] ; then
      echo "Docker container is up and running!"
  else
      echo -e "${RED}Docker container failed to start or there is no connection!${NC}"; exit 0;
  fi
}
docker_check;
docker_compose_check;
clear;
main;
exit 0;
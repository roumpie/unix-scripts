#!/bin/bash

#Variables
db_name="container_name" 
db_user="postgres" 
#############################
ssh_server=server_ip
ssh_user=root
ssh_key=ssh_key_path
ssh_port=22
#############################
temporary_dir="/tmp/postgres_db/" #to be changed.
backup_dir="tmp/$ssh_server"
echo "Setting up $backup_dir"

   if [ ! -d "$backup_dir" ]; then
       echo "Creating $backup_dir"
       mkdir -p "$backup_dir"
   fi

echo "----------------------------------#-------------------------------------"
echo "Script to download postgreSQL backup from host and restore it locally"
echo "----------------------------------#-------------------------------------"
echo

function backup () {


	#run the following commands over ssh:
	#pg_dumpall to backup all the databases in our dockerized postgreSQL
	backup_postgre="mkdir -p $temporary_dir ;sudo docker exec -u postgres $db_name pg_dumpall -c > $temporary_dir$db_name-backup.sql.gz"
	ssh -i george -t -p ${ssh_port} $ssh_user@$ssh_server -o ConnectTimeout=10 $backup_postgre
	if [ $? = 0 ]
	then
		echo "Backup successful ";
	elif [ $? = 255 ]
		then
			echo "Host unreachable! Error code: $?"
	else
		echo "There was an error aborting... Error code: $?"
		exit 1;
	fi
	echo "----------------------------------#-------------------------------------"

}
function donwload_backup () {
	#Get the file genereted by the previews ssh conn:
	scp -i george -o ConnectTimeout=10 $ssh_user@$ssh_server:"$temporary_dir$db_name-backup.sql.gz" $backup_dir
	if [ $? = 0 ]
	then
		echo "Backup downloaded successfully ";
	else
		echo "There was an error aborting...  Error code: $?"
		exit 1;
	fi
	echo "----------------------------------#-------------------------------------"
	#rename the file to enter the date/
	chmod +x "$backup_dir/$db_name-backup.sql.gz"
	mv $backup_dir/$db_name-backup.sql.gz $backup_dir/$db_name-backup-$(date -u +%Y-%m-%d_%H-%M).sql.gz
}

function delete_from_host () {
#reconnect to delete the generated folder from the host
rm_files="rm -r $temporary_dir"
ssh -i george -t -p ${ssh_port} $ssh_user@$ssh_server -o ConnectTimeout=10 sudo $rm_files
if [ $? = 0 ]
then
	echo "Removed files from host";
elif [ $? = 255 ]
	then
		echo "Host unreachable Error code: $?"
else
	echo "There was an error aborting... Error code: $?"
	exit 1;
fi
echo "----------------------------------#-------------------------------------"
}

function restore_backup () {
#Restore the back up on local pc: 
backup_name="$(find $backup_dir -type f -exec basename {} \; | sort -r | head -1)"
#restore_postgre="cat $backup_dir/$backup_name | docker exec -i $db_name psql -u $db_user"
cat $backup_dir/$backup_name | docker exec -i $db_name psql -U $db_user
#$restore_postgre-------- add sudo if needed

if [ $? = 0 ]
then
	echo "DB successfully restored ";
else
	echo "An error accured while restoring postgreSQL!!! Error code: $?"
		echo "----------------------------------#-------------------------------------"
		echo "Retrying with previous backup..."

	backup_name=$(find $backup_dir -type f -exec basename {} \; | sort -r | head -2 | tail -n 1)
	#$restore_postgre -------- add sudo if needed
	cat $backup_dir/$backup_name | docker exec -i $db_name psql -U $db_user
	if [ $? = 0 ]
     then
		echo "DB successfully restored ";
     else
		echo "An error accured while restoring postgreSQL!!! Error code: $?"
	exit 1;
fi

fi
echo "----------------------------------#-------------------------------------"
}

#func to delete backups older than specific date to save space
function delete_old_backups () {
	delete_after_days="+10";
    find $backup_dir -mtime $delete_after_days -type f -delete
}


backup;
donwload_backup;
delete_from_host;
restore_backup;
exit 0;